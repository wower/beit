<img src= "images/ProjectLogo.png" width="400">

# BEIT - Building Energy Intensity Toolchain
Be the change you want to be. 

# Key Question
How does building data turn into actionable knowledge?

# Problem Description
Buildings consume a lot of energy and produce a lot of data. There are many ways to analyze this data, and there is no straightforward path to quantify a building's performance in an integrated manner. This presents a problem for decision-makers because it creates a gap between the overwhelming amount of building management data being produced and meaningful ways to draw insights from it.

Excellent work has been done modelling the potential retrofits opportunities of the existing building stock to reduce energy consumption and GHG emissions. This approach, however, neglects opportunities and advances in data science to assist in the operations of a property to ensure the building is always running optimally.

Products that address this gap need to be data-driven, and offer a value chain which connects different types of analysis to inform the decision-making process when running a building, considering upgrades, or finding efficiencies.

# Project Team
| <img src= "images/Team-Victoria.png" width="300">    | <img src= "images/Team-Austin.png" width="200">  |
| :---:       |   :---:      |   
| Blair Birdsell        |   Praveen Radhakrishnan  |
| Rajeev Kotha          |  Kingsley Nweye          | 
| Liam Jowett-Lockwood  |                          | 
| François Lédée        |                          | 
| Dave Rulff            |                          | 

# Summary
The strength of the proposed **Building Energy Intensity Toolchain** is that it combines several types of analysis to deliver meaningful insights into the physical or operational characteristics of a building. These insights can assist in drawing conclusions within the decision-making process.

Phase one of the toolchain uses energy consumption data to quantify a building's performance. This data is used to 1) compare the building against an archetype simulation of the building type and 2) measure the building's performance against the location's weather data. Phase two analyzes the available building data to uncover the possibility of potential electrification, and to identify potential equipment and sensor faults.

BEIT meets users needs by taking unfiltered data and condensing it into meaningful and actionable conclusions presented in a dashboard. Its modular design means that as further innovation and development occurs, corresponding upgrades to each module can increase the performance and functionality of the toolchain.

<img src= "images/ProjectSummary.png" width="700">